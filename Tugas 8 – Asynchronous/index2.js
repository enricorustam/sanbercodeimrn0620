var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
let index = 0

function FNreadbook(waktu){
    if (index<books.length){
        readBooksPromise(waktu, books[1])
        .then(result => FNreadbook(result))
        .catch(e => console.log(e))
    }
    index = index + 1
}

FNreadbook(100000)