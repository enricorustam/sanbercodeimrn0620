console.log("No. 1")
function teriak() {
    return "Halo Sanbers!";
  }

console.log(teriak());
console.log("")

console.log("No. 2")
function kalikan(x, y) {
    return x * y;
  }
 
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48
console.log("")

console.log("No. 3")
function introduce(n, a, add, h) {
    return "Nama Saya "+n+", umur saya "+a+" tahun, alamat saya di "+add+", dan saya punya hobby yaitu "+h+"!";
  }
 
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 