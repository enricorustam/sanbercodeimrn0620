import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity 
} from 'react-native';

export default class Reg extends Component{

	render(){
		return(
			<View style={styles.container}>
           <TouchableOpacity style={styles.button}>
             <Text style={styles.buttonText}>{this.props.type}</Text>
           </TouchableOpacity>     
  		</View>
			)
	}
}

const styles = StyleSheet.create({
  container : {
    flexGrow: 1,
    justifyContent:'flex-start',
    alignItems: 'center'
  },

  inputBox: {
    width:300,
    borderRadius: 25,
    paddingHorizontal:16,
    fontSize:16,
    borderColor: '#003366',
    color:'#ACACAC',
    marginVertical: 10
  },
  button: {
    width:300,
    backgroundColor:'#1c313a',
     borderRadius: 25,
      marginVertical: 10,
      paddingVertical: 13
  },
  buttonText: {
    fontSize:16,
    fontWeight:'500',
    color:'#FFFFFF',
    textAlign:'center'
  }
  
});