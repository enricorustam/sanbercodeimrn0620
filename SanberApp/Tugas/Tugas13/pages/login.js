import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar ,
  TouchableOpacity
} from 'react-native';

import Logo from '../components/logo';
import Form from '../components/form';
import Reg from '../components/Registerbtn';

//import {Actions} from 'react-native-router-flux';

export default class Login extends Component{

	//signup() {
	//	Actions.signup()
	//}

	render() {
		return(
			<View>
                {/* style={styles.container} */}
				<Logo/>
				<Form type="Login"/>
                
				<View style={styles.signupTextCont}>
					<Text style={styles.signupText}>atau</Text>
					<TouchableOpacity onPress={this.signup}><Text style={styles.signupButton}> Signup</Text></TouchableOpacity>
				</View>
                <Reg type="Register"/>
			</View>	
			)
	}
}

const styles = StyleSheet.create({
    // container : {
    //     backgroundColor:'#455a64',
    //     flex: 1,
    //     alignItems:'center',
    //     justifyContent :'center'
    //   },
      signupTextCont : {
        //flexGrow: 1,
        alignItems:'flex-start',
        justifyContent :'center',
        paddingVertical:1,
        flexDirection:'row'
      },
      signupText: {
          color:'#ACACAC',
          fontSize:16
      },
      signupButton: {
          color:'#ffffff',
          fontSize:16,
          fontWeight:'500'
      }
});