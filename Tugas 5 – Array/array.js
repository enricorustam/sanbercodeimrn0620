// NO. 1
console.log("No. 1")

function range(startNum, finishNum){

    arr=[]

    if (startNum == null){
        return -1;
    }
    else if (finishNum == null){
        return -1;
    }
    else if (startNum == null && finishNum == null){
        return -1;
    }
    else if ( startNum < finishNum ){
        for(i = startNum; i <= finishNum;i++){
            arr.push(startNum);
            startNum = startNum + 1;
        }
        return arr;
    }
    else if ( startNum > finishNum ){
        for(i = startNum; i >= finishNum;i--){
            arr.push(startNum);
            startNum = startNum - 1;
        }
        return arr;
    }
}
 
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log("")

// NO. 2
console.log("No. 2")
function rangeWithStep(startNum, finishNum, step) {

    arr = []

    if ( startNum < finishNum ){
        for(i = startNum; i <= finishNum;i = i + step){
            arr.push(startNum);
            startNum = startNum + step;
        }
        return arr;
    }
    else if ( startNum > finishNum ){
        for(i = startNum; i >= finishNum;i = i - step){
            arr.push(startNum);
            startNum = startNum - step;
        }
        return arr;
    }

}

 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log("")

// NO. 3
console.log("No. 3")
function sum(startNum, finishNum, step) {

    if (step == null && startNum < finishNum){
        var jumlah = 0;
        for (i = startNum; i <= finishNum; i++) {
            
            jumlah = jumlah + i;
         }
         return jumlah;
    }

    else if (step == null && startNum > finishNum){
        var jumlah = 0
        for (i = startNum; i >= finishNum; i--) {
            
            jumlah = jumlah + i;
         }
         return jumlah
    }

    else if ( startNum < finishNum ){
        var hasil = 0;

        for(i = startNum; i <= finishNum;i = i + step){
            hasil = hasil + i;
            //startNum = startNum + step;
        }
        return hasil;
    }

    else if ( startNum > finishNum ){
        var hasil2 = 0;
        for(i = startNum; i >= finishNum;i = i - step){
            hasil2 = hasil2 + i;
        }
        return hasil2;
    }

    else if (step == null && finishNum == null){

        return startNum;
    }

    else if(step == null && finishNum == null && startNum == null){
        nol = 0
        return nol;
    }
   return '\n';

}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log("")

// NO. 4
console.log("No. 4")

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
function datahandling(input){
    for(var i = 0; i < input.length; i++) { 
        console.log('nomor ID : '+input[i][0]) ; 
        console.log('Nama : '+input[i][1]) ; 
        console.log('TTL : '+input[i][2] +' '+ input[i][3]) ; 
        console.log('Hobi : '+input[i][4]) ; 
        console.log('') ; 
    } 
}

datahandling(input);
 
console.log("")

// NO. 5
console.log("No. 5")

function balikKata(kata){

    hasil = "";

    for(i = kata.length - 1; i >=0; i--){
        hasil += kata[i];
    }

    return hasil;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log("")

// NO. 6
console.log("No. 6")

var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]

//var tambah = "Pria", "SMA Internasional Metro"

function datahandling2(input){
    input.splice(4,1,"Pria", "SMA Internasional Metro")
    console.log (input)

    var tanggal = input[3]
    var splitTgl = tanggal.split("/")
    console.log(splitTgl)
    var hbulan = splitTgl[1]
    var bulan = parseInt(hbulan, 10)
  
    switch(bulan){
        case 1:
            bulan = "Januari"
            break
        case 2:
            bulan = "Februari"
            break
        case 3:
            bulan = "Maret"
            break
        case 4:
            bulan = "April"
            break
        case 05:
            bulan = "Mei"
            break
        case 6:
            bulan = "Juni"
            break
        case 7:
            bulan = "Juli"
            break
        case 8:
            bulan = "Agustus"
            break
        case 9:
            bulan = "September"
            break
        case 10:
            bulan = "Oktober"
            break
        case 11:
            bulan = "November"
            break
        case 12:
            bulan = "Desember"
            break
    }

    console.log(bulan)

    //rev = splitTgl.reverse()
    //console.log(rev)
    
    tgl = splitTgl.join("-")
    console.log(tgl)

    nama = input[1]
    var potongNama = nama.slice(0,15)
    console.log(potongNama)
}
datahandling2(input2);

console.log("")