
console.log("Nomor 1")
let now = new Date()
var thisyear = now.getFullYear()

function arrayToObject(arr) {
    for (i = 0; i<arr.length;i++){
        var arrobj = {
            firstname : arr[i][0],
            lastname : arr[i][1],
            gender : arr[i][2],
            age : thisyear - arr[i][3],
                
        }
        if (arr.length == 0){
            return ""
        }
        else if(isNaN(arrobj.age) || arrobj.age < 0){
            arrobj.age = "invalid Birth Year"
            return console.log((i+1)+".",arrobj.firstname,'',arrobj.lastname,':',arrobj)
        }
        else{
            console.log((i+1)+".",arrobj.firstname,'',arrobj.lastname,':',arrobj)
        }

    } 
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

console.log("")


console.log("Nomor 2")
function shoppingTime(memberId, money) {
    // you can only write your code here!
    if(!memberId){
        return 'Mohon maaf, toko X hanya berlaku untuk member saja';
    } else if(money < 50000){
        return 'Mohon maaf, uang tidak cukup'
    } else {
        var listBarang = [['Sepatu Stacattu', 1500000], ['Baju Zoro', 500000], ['Baju H&N', 250000], ['Sweater Uniklooh', 175000], ['Casing Handphone', 50000]];
        var member ={};
        member.memberId = memberId;
        member.money = money;
        member.listPurchased = [];
        for (let i = 0; i < listBarang.length; i++) {
            if(money >= listBarang[i][1]){
                member.listPurchased.push(listBarang[i][0]);
                money -= listBarang[i][1];
            }
        }
        member.changeMoney = money;
        return member;
    }

  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

  console.log("")

  console.log("Nomor 3")
  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var data = [];
    for (let i = 0; i < arrPenumpang.length; i++) {
        var penumpang = arrPenumpang[i][0];
        data.push({penumpang});
        data[i].naikDari = arrPenumpang[i][1];
        data[i].tujuan = arrPenumpang[i][2];
        var indexAsal;
        for (let j = 0; j < rute.length; j++) {
            if(data[i].naikDari === rute[j]){
                indexAsal = j;
            }
        }
        var indexTujuan;
        for (let j = 0; j < rute.length; j++) {
            if(data[i].tujuan === rute[j]){
                indexTujuan = j;
            }
        }
        if(indexAsal > indexTujuan) { //Kalau naik arah sebaliknya
            var temp = indexAsal;
            indexAsal = indexTujuan;
            indexTujuan = temp;
        }
        data[i].bayar = (indexTujuan-indexAsal)*2000;
    }
    return data;
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]